const isPalindrome = (str) => {
  let text = str.toLowerCase();
  let reversed = "";

  // reverse the string and set it into reversed variable
  for (let i = str.length - 1; i >= 0; i--) {
    reversed += text[i];
  }

  //check if it's same then return true
  return text == reversed ? true : false;
};

console.log(isPalindrome("abcba"));
console.log(isPalindrome("ABCD"));
