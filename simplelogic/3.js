const arr = ['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e']


const group = (arrGroup) => {

    //result start with array index 0
    var result = [[arrGroup[0]]]

    //start loop from index 1
    arrGroup.slice(1).forEach((item,index) => {
      if(item == arrGroup[index]) {
    //iterate trough multidimensional array
        result[result.length-1].push(item);
      } else{ 
        //else not same push new array
        result.push([item]);
      }
    });
    return result;
}


console.log(group(arr))


