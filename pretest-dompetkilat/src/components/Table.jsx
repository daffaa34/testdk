import React, { Component } from 'react';
import { Row, Col, Spin, Tag, Input, Select } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import CarouselComponent from './Carousel';
import ConInv from '../json/invoice/conventional_invoice.json';
import ProInv from '../json/invoice/productive_invoice.json';
import ProOsf from '../json/osf/productive_osf.json';
import ConOsf from '../json/osf/conventional_osf.json';
import Reksadana from '../json/reksadana/reksadana.json';
import Sbn from '../json/sbn/sbn.json';

class TableData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      tableHead: [],
      isSBN: false,
      isReksa: false,
      dataField: [],
      limit: 0,
      hasItem: true,
      initialLoad: true,
      filterParam: [],
      search: '',
      dropdown: 120
    };

    this.handleFetch();
  }

  componentDidMount() {
    this.fetchData();
  }

  handleFetch = () => {
    let data = null;
    const { location } = this.props;
    const { tableHead, dataField, filterParam } = this.state;
    if (location.name === 'SBN') {
      data = Sbn;
      this.setState({ isSBN: true, isReksa: false });
      tableHead.push('Name', 'Amount', 'Tenor', 'Rate', 'Type', 'Sub');
      filterParam.push('ST', 'SBR');
    } else if (location.name === 'Conventional Invoice') {
      data = ConInv;
      tableHead.push('Name', 'Amount', 'Tenor', 'Grade', 'Rate', 'Sub');
      this.setState({ isSBN: false, isReksa: false });
      filterParam.push('A', 'B+', 'B');
    } else if (location.name === 'Productive Invoice') {
      data = ProInv;
      tableHead.push('Name', 'Amount', 'Tenor', 'Grade', 'Rate', 'Sub');
      this.setState({ isSBN: false, isReksa: false });
      filterParam.push('A', 'B+', 'B');
    } else if (location.name === 'Conventional OSF') {
      data = ConOsf;
      tableHead.push('Name', 'Amount', 'Tenor', 'Grade', 'Rate', 'Sub');
      filterParam.push('A', 'B+', 'B');
      this.setState({ isSBN: false, isReksa: false });
    } else if (location.name === 'Productive OSF') {
      data = ProOsf;
      tableHead.push('Name', 'Amount', 'Tenor', 'Grade', 'Rate', 'Sub');
      filterParam.push('A', 'B+', 'B');
      this.setState({ isSBN: false, isReksa: false });
    } else if (location.name === 'Reksadana') {
      data = Reksadana;
      this.setState({ isReksa: true });
      tableHead.push('Name', 'Amount', 'Return', 'Sub');
      filterParam.push('Negative', 'Positive');
    }
    dataField.push(data);
  }

  handleFilter = async (s) => {
    // const { search } = this.state;
    await this.setState({ dropdown: s });
    this.fetchData();
  }

  handleSearch = async (s) => {
    await this.setState({ search: s.target.value });
    this.fetchData();
  }

  fetchData = () => {
    const { limit, isLoading, dataField, hasItem } = this.state;
    const lenAll = dataField[0].contents.length;
    const len = dataField[0].contents.slice(0, limit).length;
    if (lenAll - len !== 0 && hasItem === true) {
      this.setState(
        { limit: limit + 5, isLoading: !isLoading, initialLoad: false, hasItem: true }
        );
      setTimeout(() => {
        this.setState({ isLoading: false });
      }, 3000);
    } else {
      this.setState({ hasItem: false });
    }
  }

  render() {
    const {
      isLoading, tableHead, isSBN, isReksa, dataField,
      limit, hasItem, filterParam, search, dropdown
    } = this.state;
    const { location } = this.props;
    const { initialLoad } = this.state;
    // console.log(location);
    const { Option } = Select;
    if (location.name === undefined) {
      window.location.href = window.location.origin;
    }
    return (
    <>
      <CarouselComponent />
      <Row justify="center" align="bottom">
      <Col xl={6} md={6} sm={5} span={4} />
      <Col xl={12} md={12} sm={14} span={16}>
      <h1>{location.name}</h1>
        {isReksa ? <><h3>Filter by Name</h3><span style={{ color: 'grey' }}>Example : INB, SBR XXX</span></> : <><h3>Filter by Name, Tenor, Rate</h3><span style={{ color: 'grey' }}>Example : INB, SBR XXX</span> </>}
                <div style={{ display: 'flex' }}>
                  <Input defaultValue="" placeholder="Search..." onChange={(e) => this.handleSearch(e)} />
                    <Select defaultValue="None" onChange={(value) => this.handleFilter(value)}>
                    <Option value={120}>None</Option>)
                      {filterParam.map((item, index) => (
                      <Option value={isReksa ? index : item}>{item}</Option>
                      ))}
                    </Select>
                </div>
            <div className="ant-table-wrapper">
              <div className="ant-spin-nested-loading">
                <div className={`ant-spin ${isLoading ? 'ant-spin-spinning' : null}`}>
                  <Spin />
                </div>
                <div className={`ant-spin-container ${isLoading ? 'ant-spin-blur' : null}`}>
                  <div className="ant-table">
                    <div className="ant-table-container" style={{ maxHeight: '300px', overflowY: 'scroll', display: 'block' }}>
                      <InfiniteScroll
                        initialLoad={initialLoad}
                        pageStart={0}
                        hasMore={hasItem}
                        useWindow={false}
                        loadMore={this.fetchData}
                      >
                        <table>
                        <thead className="ant-table-thead">
                          <tr>
                        {
                        isSBN || isReksa ? tableHead.map((item) => <th className="ant-table-cell">{item}</th>) : tableHead.map((item) => <th className="ant-table-cell">{item}</th>)
                        }
                          </tr>
                        </thead>
                        <thead className="ant-table-tbody">
                            {
                              tableHead.length === 4
                              ? dataField[0].contents.slice(0, limit)
                              .filter((el) => (search === '' ? el : el.name === search))
                              .map((item) => (
                                dropdown === 120
                                ? <>
                                    <tr>
                                      <td className="ant-table-cell">{item.name}</td>
                                      <td className="ant-table-cell">{item.amount}</td>
                                      <td className="ant-table-cell">{item.return}</td>
                                      <td className="ant-table-cell">{item.sub == null ? <Tag color="#2db7f5">Sub not found</Tag> : null}</td>
                                    </tr>
                                  </> : dropdown === 'Negative' && item.return < 0
                                  ? <>
                                      <tr>
                                        <td className="ant-table-cell">{item.name}</td>
                                        <td className="ant-table-cell">{item.amount}</td>
                                        <td className="ant-table-cell">{item.return}</td>
                                        <td className="ant-table-cell">{item.sub == null ? <Tag color="#2db7f5">Sub not found</Tag> : null}</td>
                                      </tr>
                                    </> : dropdown === 'Positive' && item.return >= 0
                                  ? <>
                                      <tr>
                                        <td className="ant-table-cell">{item.name}</td>
                                        <td className="ant-table-cell">{item.amount}</td>
                                        <td className="ant-table-cell">{item.return}</td>
                                        <td className="ant-table-cell">{item.sub == null ? <Tag color="#2db7f5">Sub not found</Tag> : null}</td>
                                      </tr>
                                    </> : null
                              )) : dataField[0].contents.slice(0, limit)
                              .filter((el) => (search === '' ? el : el.rate.toString() === search || search === '' ? el : el.tenor.toString() === search
                                || search === '' ? el : el.name.toString() === search
                              ))
                              .map((item) => (
                                item.tenor === dropdown
                                ? <>
                                      <tr>
                                        <td className="ant-table-cell">{item.name}</td>
                                        <td className="ant-table-cell">{item.amount}</td>
                                        <td className="ant-table-cell">{item.tenor}</td>
                                        {item.grade != null ? <td className="ant-table-cell">{item.grade}</td> : <td className="ant-table-cell">{item.rate}</td>}
                                        {item.type != null ? <td className="ant-table-cell">{item.type}</td> : <td className="ant-table-cell">{item.rate}</td>}
                                        <td className="ant-table-cell">{item.sub == null ? <Tag color="magenta">Sub not found</Tag> : null}</td>
                                      </tr>
                                  </>
                                : item.type === dropdown
                                ? <>
                                      <tr>
                                        <td className="ant-table-cell">{item.name}</td>
                                        <td className="ant-table-cell">{item.amount}</td>
                                        <td className="ant-table-cell">{item.tenor}</td>
                                        {item.grade != null ? <td className="ant-table-cell">{item.grade}</td> : <td className="ant-table-cell">{item.rate}</td>}
                                        {item.type != null ? <td className="ant-table-cell">{item.type}</td> : <td className="ant-table-cell">{item.rate}</td>}
                                        <td className="ant-table-cell">{item.sub == null ? <Tag color="magenta">Sub not found</Tag> : null}</td>
                                      </tr>
                                  </>
                                  : item.grade === dropdown
                                  ? <>
                                      <tr>
                                        <td className="ant-table-cell">{item.name}</td>
                                        <td className="ant-table-cell">{item.amount}</td>
                                        <td className="ant-table-cell">{item.tenor}</td>
                                        {item.grade != null ? <td className="ant-table-cell">{item.grade}</td> : <td className="ant-table-cell">{item.rate}</td>}
                                        {item.type != null ? <td className="ant-table-cell">{item.type}</td> : <td className="ant-table-cell">{item.rate}</td>}
                                        <td className="ant-table-cell">{item.sub == null ? <Tag color="magenta">Sub not found</Tag> : null}</td>
                                      </tr>
                                    </> : null
                                ))
                            }
                        </thead>
                        </table>
                      </InfiniteScroll>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      </Col>
      <Col xl={6} sm={5} md={6} span={4} />
      </Row>
    </>
    );
  }
}
export default TableData;
