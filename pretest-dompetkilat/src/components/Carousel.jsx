import React, { Component } from 'react';
import { Row, Col, Carousel } from 'antd';
import Banner from '../assets/banner.png';
// import ConInv from '../json/invoice/conventional_invoice.json';
// import ProInv from '../json/invoice/productive_invoice.json';
// import ProOsf from '../json/osf/productive_osf.json';
// import ConOsf from '../json/osf/conventional_osf.json';
// import Reksadana from '../json/reksadana/reksadana.json';
// import Sbn from '../json/sbn/sbn.json';

class CarouselComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    const contentStyle = {
      height: '160px',
      color: '#fff',
      textAlign: 'center',
      background: '#364d79'
    };
    return (
      <Row justify="center" align="bottom">
        <Col xl={6} md={6} sm={5} span={4} />
        <Col xl={12} md={12} sm={14} span={16}>
            <Carousel autoplay>
              <div style={contentStyle}>
                <img style={{ width: '100%' }} src={Banner} alt="Banner" />
              </div>
              <div style={contentStyle}>
                <img style={{ width: '100%' }} src={Banner} alt="Banner" />
              </div>
            </Carousel>
        </Col>
          <Col xl={6} sm={5} md={6} span={4} />
      </Row>
    );
  }
}
export default CarouselComponent;
