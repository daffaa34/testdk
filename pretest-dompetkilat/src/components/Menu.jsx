import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Spring } from 'react-spring/renderprops';
import { Row, Col, Badge } from 'antd';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileContract } from '@fortawesome/free-solid-svg-icons';

const Menu = (props) => {
    const [hover, setHover] = useState(false);

    const setCol = () => {
        setHover(!hover);
      };

    const { content } = props;
    const box = {
        backgroundColor: hover ? '#eb2f06' : 'white',
        color: hover ? 'white' : 'black',
        padding: '1.5rem',
        marginTop: 5,
        marginBottom: 5,
        border: '1px solid #CAD5E2'
    };
    return (
      <Row justify="center" align="bottom">
        <Col xl={6} md={6} sm={5} span={4} />
        <Col xl={12} md={12} sm={14} span={16}>
          <Spring
            from={{ opacity: 0 }}
            to={{ opacity: 1 }}
            config={{ delay: 1000, duration: 1000 }}
          >
            {(style) => (
              <div style={style}>
              <Link href="/#" to={{ pathname: '/Table', ...props }} style={{ color: 'black' }}>
                <div style={box} onMouseEnter={() => setCol()} onMouseLeave={() => setCol()}>
                  <Row>
                    <Col sm={8} xl={4} md={10}>
                      <div style={{ paddingTop: '25%', paddingLeft: '10%' }}>
                        <FontAwesomeIcon icon={faFileContract} size="8x" />
                      </div>
                    </Col>
                    <Col xl={17} md={20}>
                      <div style={{ position: 'relative' }}>
                        <h1 style={{ color: hover ? 'white' : 'black' }}>{props.name}</h1>
                        <p>{content}</p>
                      </div>
                    </Col>
                    <Col span={2} sm={1}>
                      <div style={{ paddingLeft: '80%' }}>
                        <Badge count={props.count} style={{ backgroundColor: hover ? 'white' : null, color: hover ? 'black' : null }} />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Link>
              </div>
            )}
          </Spring>
        </Col>
        <Col xl={6} sm={5} md={6} span={4} />
      </Row>
    );
  };

  Menu.propTypes = {
    item: PropTypes.shape({}).isRequired,
    content: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired
  };

  export default Menu;
