import React, { useState, useEffect } from 'react';
import { Spring, Transition, animated } from 'react-spring/renderprops';
import { Row, Col, Badge } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileContract } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Invoice from '../json/invoice/invoice.json';
import Osf from '../json/osf/osf.json';

export const SubMenu = (props) => {
  const [toogle, setToogle] = useState(false);
  const [hover, setHover] = useState(false);

  const setOpen = () => {
    setToogle(!toogle);
  };

  const setCol = () => {
    setHover(!hover);
  };

  const { content } = props;
  const box = {
    backgroundColor: toogle || hover ? '#eb2f06' : 'white',
    color: toogle || hover ? 'white' : 'black',
    padding: '1.5rem',
    marginTop: 5,
    marginBottom: 5,
    border: '1px solid #bdc3c7'
  };
  return (
    <Row justify="center" align="bottom">
        <Col xl={6} md={6} sm={5} span={4} />
      <Col xl={12} md={12} sm={14} span={16}>
        <Spring
          from={{ opacity: 0 }}
          to={{ opacity: 1 }}
          config={{ delay: 500, duration: 1000 }}
        >
          {(style) => (
            <div style={style}>
              <div style={box} onClick={() => setOpen()} onKeyDown={() => setOpen()} role="button" tabIndex={0} onMouseEnter={() => setCol()} onMouseLeave={() => setCol()}>
                <Row>
                  <Col sm={8} xl={4} md={10}>
                    <div style={{ paddingTop: '25%', paddingLeft: '10%' }}>
                      <FontAwesomeIcon icon={faFileContract} size="8x" />
                    </div>
                  </Col>
                  <Col xl={17} md={20} sm={12}>
                    <div style={{ position: 'relative' }}>
                      <h1 style={{ color: toogle || hover ? 'white' : 'black' }}>{props.name}</h1>
                      <p>{content}</p>
                    </div>
                  </Col>
                  <Col span={2} sm={1}>
                    <div style={{ paddingLeft: '80%' }}>
                      <Badge count={props.count} style={{ backgroundColor: toogle || hover ? 'white' : null, color: toogle || hover ? 'black' : null }} />
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          )}
        </Spring>
        <Transition
          native
          items={toogle}
          from={{ opacity: 0 }}
          enter={{ opacity: 1 }}
          leave={{ opacity: 0 }}
        >
          {(show) => show && ((div) => (
            <animated.div style={div}>
              <DropMenu name={props.name} content={props.content} count={props.count} />
            </animated.div>
          ))}
        </Transition>
      </Col>
      <Col xl={6} sm={5} md={6} span={4} />
    </Row>
  );
};

SubMenu.propTypes = {
  item: PropTypes.shape({}).isRequired,
  content: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
};

export const DropMenu = (props) => {
  const [data, setData] = useState([]);

  const { name, content } = props;
  const type = name.substr(0, 3).toLowerCase();

  useEffect(() => {
      const checkType = () => {
        if (type === 'inv') {
            setData(Invoice.contents);
        } else if (type === 'osf') {
            setData(Osf.contents);
        }
    };
      checkType();
  }, []);

  const box = {
    backgroundColor: 'white',
    color: 'black',
    padding: '1.5rem',
    marginTop: 5,
    marginBottom: 5,
    border: '1px solid #bdc3c7'
};
  return (
    <>
        <Row justify="center" align="bottom">
        {data != null ? data.map((item) => (
          <Col span={12}>
            <Link href="/#" to={{ pathname: '/Table', ...item }} style={{ color: 'black' }}>
                <div style={box}>
                    <Row>
                        <Col xl={5} sm={8}>
                            <div>
                                <FontAwesomeIcon icon={faFileContract} size="4x" />
                            </div>
                        </Col>
                        <Col sm={20} md={15} xl={15}>
                            <div style={{ position: 'relative' }}>
                                <h3>{item.name}</h3>
                                <p>{content}</p>
                            </div>
                        </Col>
                        <Col span={5} sm={3}>
                            <div style={{ paddingLeft: '80%' }}>
                                <Badge count={item.count} />
                            </div>
                        </Col>
                    </Row>
                </div>
            </Link>
          </Col>
            )) : null}
        </Row>
    </>
  );
};
