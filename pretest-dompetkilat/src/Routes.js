import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import App from './App';
import Table from './components/Table';

const AppRouter = () => (
  <Router>
    <Switch>
      <Route path="/" exact component={App} />
      <Route path="/Table" exact component={Table} />
    </Switch>
  </Router>
);

export default AppRouter;
