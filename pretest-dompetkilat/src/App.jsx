// eslint-disable-next-line max-len
import React, { Component } from 'react';
import { SubMenu } from './components/Submenu';
import Menu from './components/Menu';
import Main from './json/main.json';
import CarouselComponent from './components/Carousel';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [],
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat risus vitae molestie placerat. Nam pretium purus vel nunc dictum semper. Donec vitae aliquam tortor. Morbi in turpis vel ligula vulputate bibendum.'
    };
    // set sub menu parameter
    const { menu } = this.state;
    Main.contents.forEach((item, index) => {
      if (index === 0 || index === 1) {
        const data = {
          count: item.count,
          name: item.name,
          sub: item.sub,
          hasMenu: true
        };
        menu.push(data);
      } else {
        menu.push(item);
      }
    });
  }

  render() {
    const { menu, content } = this.state;
    return (
      <>
        <CarouselComponent />
        {menu.map((item) => (
          item.hasMenu
            ? <SubMenu content={content} name={item.name} count={item.count} />
            : <Menu content={content} name={item.name} count={item.count} />
        ))}
      </>
    );
  }
}

export default App;
