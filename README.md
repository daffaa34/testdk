# TestDK

1. Simple Logic Test
2. pretest-dompetkilat (Web App) 

# For simple logic you can compile the code using this link

https://repl.it/languages/javascript

# For web app you can access using this link

https://testdk.netlify.app/

# For local use (Web)

* Make sure you have installed nodejs

1. Open the terminal/cmd
2. Clone this project (git clone https://gitlab.com/daffaa34/testdk.git)
3. Change dir to pretest-dompetkilat (cd pretest-dompetkilat)
4. Install the module (npm install)
5. Run the project (npm start)

# For Simple Logic 

* Make sure you have installed nodejs

1. Open the terminal/cmd
2. Clone this project (git clone https://gitlab.com/daffaa34/testdk.git)
3. Change dir to simplelogic (cd simplelogic)
4. Run the file (node filename)
example : node 1